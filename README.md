# A hexdump/hd-like tool that displays the data in real time

Most of the `hexdump`/`hd`/`xxd`/`od` tools want to read a whole set of
eight or sixteen characters before outputting anything, and for most of
them it makes sense. Sometimes, however, it would be useful to dump
the output of a tool or the data read from a file descriptor in real time,
without buffering.

That's where the `hd-realtime` tool comes in. It is currently extremely
simplistic: run with no arguments, it reads data from the standard input
stream and formats something on its standard output. Currently it uses
ANSI escape sequences; maybe some day it may be taught to use termcap,
terminfo, ncurses, or something similar.

There is also an `outseq` sample program that outputs bytes in sequences of
one, then two, then three, at one-second intervals, to simulate data
arriving in bursts.

Demo run:

    cargo build --release
    cargo run -q --release --bin outseq | cargo run -q --release --bin hd-realtime

    # or, most probably equivalent to the last command (once built):
    ./target/release/outseq | ./target/release/hd-realtime

Pending: document it, make it a bit more configurable :)

Comments: Peter Pentchev <[roam@ringlet.net](mailto:roam@ringlet.net)>
