#![warn(missing_docs)]
//! Output a sequence of bytes/characters/whatever.
/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::io::{self, Write};
use std::iter;
use std::thread;
use std::time;

use expect_exit::ExpectedWithError;

const CHUNKS: [usize; 3] = [1, 2, 3];

fn main() {
    let mut values = iter::successors(Some(64), |value| match value {
        131 => Some(0),
        other => Some(other + 1),
    });
    let stdout_unlocked = io::stdout();
    let mut stdout = stdout_unlocked.lock();
    let second = time::Duration::from_secs(1);

    for count in CHUNKS.iter().cloned().cycle() {
        let buf: Vec<u8> = (0..count).map(|_| values.next().unwrap()).collect();
        stdout
            .write(&buf[..])
            .or_exit_e(|| format!("Could not write {:?} to stdout", &buf[..count]));
        stdout.flush().or_exit_e_("Could not flush stdout");
        thread::sleep(second);
    }
}
