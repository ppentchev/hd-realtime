//! Simulate hd/hexdump for data read in real time.
/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::env;
use std::io::{self, Read};

use expect_exit::ExpectedWithError;

use hd_realtime::format;
use hd_realtime::output;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 1 {
        expect_exit::exit("Usage: hd-realtime\nData read via the standard input stream.");
    }

    let stdout_unlocked = io::stdout();
    let stdout = stdout_unlocked.lock();
    let output = output::AnsiOutput::new(stdout);
    let mut state = format::State::new(output);
    state = state
        .start_output()
        .or_exit_e_("Could not start outputting data");

    let stdin_unlocked = io::stdin();
    let mut stdin = stdin_unlocked.lock();
    loop {
        let mut buf = [0; 1024];
        let count = stdin
            .read(&mut buf[..])
            .or_exit_e_("Could not read from the standard input stream");
        if count == 0 {
            break;
        }
        state = state
            .handle_input(&buf[..count])
            .or_exit_e_("Could not process input");
    }
}
