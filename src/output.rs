//! Helper functions for moving around a screen line.
//!
//! Only implemented using ANSI escape sequences for the present.

/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::error;
use std::io::Write;

/// Move around a screen line and output text.
pub trait Output {
    /// Display the specified string and move the cursor accordingly.
    fn display(&mut self, msg: &str) -> Result<(), Box<dyn error::Error>>;

    /// Move to the specified column on the current line.
    fn move_to_column(&mut self, column: usize) -> Result<(), Box<dyn error::Error>>;

    /// Move to the start of the next line.
    fn new_line(&mut self) -> Result<(), Box<dyn error::Error>>;
}

/// Move around a screen line and output text.
#[derive(Debug)]
pub struct AnsiOutput<W: Write> {
    stream: W,
    pos: usize,
}

impl<W> AnsiOutput<W>
where
    W: Write,
{
    /// Initialize the structure with the specified output stream.
    pub fn new(stream: W) -> Self {
        Self { stream, pos: 0 }
    }

    /// Borrow a reference to the output stream.
    pub fn stream_ref(&self) -> &W {
        &self.stream
    }
}

impl<W> Output for AnsiOutput<W>
where
    W: Write,
{
    fn display(&mut self, msg: &str) -> Result<(), Box<dyn error::Error>> {
        self.stream.write_all(msg.as_ref())?;
        self.stream.flush()?;
        self.pos += msg.len();
        Ok(())
    }

    fn move_to_column(&mut self, column: usize) -> Result<(), Box<dyn error::Error>> {
        if self.pos != column {
            let cmd = match self.pos < column {
                true => format!("\x1b[{}C", column - self.pos),
                false => format!("\x1b[{}D", self.pos - column),
            };
            self.stream.write_all(cmd.as_ref())?;
            self.stream.flush()?;
            self.pos = column;
        }
        Ok(())
    }

    fn new_line(&mut self) -> Result<(), Box<dyn error::Error>> {
        self.stream.write_all(b"\n")?;
        self.stream.flush()?;
        self.pos = 0;
        Ok(())
    }
}
