//! Format input data and send it to the output handler.

/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::error;

use crate::output;

const WIDTH: usize = 16;

/// Format input data and send it to an output handler.
#[derive(Debug)]
pub struct State<O: output::Output> {
    /// The current offset in the input stream (the number of bytes processed).
    pub offset: usize,
    output: O,
}

impl<O: output::Output> State<O> {
    /// Initialize the format helper.
    pub fn new(output_handler: O) -> Self {
        Self {
            offset: 0,
            output: output_handler,
        }
    }

    /// Borrow a reference to the output handler.
    pub fn output_ref(&self) -> &O {
        &self.output
    }

    fn byte_column(column: usize) -> usize {
        8 + 2 + 3 * column + (column >= 8) as usize
    }

    fn char_column(column: usize) -> usize {
        Self::byte_column(WIDTH) + 2 + column
    }

    fn start_line(mut self) -> Result<Self, Box<dyn error::Error>> {
        if (self.offset % WIDTH) != 0 {
            panic!("Internal error: start_line({})", self.offset);
        }
        self.output.move_to_column(0)?;
        self.output.display(&format!("{:08x}  ", self.offset))?;
        self.output.move_to_column(Self::char_column(0) - 1)?;
        self.output.display("|")?;
        self.output.move_to_column(Self::char_column(WIDTH))?;
        self.output.display("|")?;
        self.output.move_to_column(Self::byte_column(0))?;
        Ok(self)
    }

    /// Start the output, e.g. display the initial offset.
    pub fn start_output(self) -> Result<Self, Box<dyn error::Error>> {
        self.start_line()
    }

    /// Process the specified input data, break it into chunks, output it.
    pub fn handle_input(mut self, data: &[u8]) -> Result<Self, Box<dyn error::Error>> {
        let col = self.offset % WIDTH;
        let rem = WIDTH - col;
        let data_len = data.len();
        let count = match data_len > rem {
            true => rem,
            false => data_len,
        };
        assert!(count > 0);

        let hex_chars = data[..count]
            .iter()
            .enumerate()
            .map(|(idx, value)| {
                format!(
                    "{:02x}{} ",
                    value,
                    match col + idx == 7 {
                        true => " ",
                        false => "",
                    }
                )
            })
            .collect::<Vec<String>>()
            .join("");
        self.output.display(&hex_chars)?;

        let human_chars = data[..count]
            .iter()
            .map(|value| match *value {
                value if (32..128).contains(&value) => char::from_u32((value).into()).unwrap(),
                _ => '.',
            })
            .collect::<Vec<char>>()
            .into_iter()
            .collect::<String>();
        self.output.move_to_column(Self::char_column(col))?;
        self.output.display(&human_chars)?;

        let next_offset = self.offset + count;
        match (next_offset % WIDTH) != 0 {
            true => {
                self.output.move_to_column(Self::byte_column(col + count))?;
                Ok(Self {
                    offset: next_offset,
                    output: self.output,
                })
            }
            false => {
                self.output.new_line()?;
                let next_line = Self {
                    offset: next_offset,
                    output: self.output,
                }
                .start_line()?;
                match data_len == count {
                    false => next_line.handle_input(&data[count..]),
                    true => Ok(next_line),
                }
            }
        }
    }
}
