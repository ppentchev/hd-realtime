//! Test the Output/AnsiOutput functionality.

/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::error;

use crate::output::{self, Output};

#[test]
fn test_ansi_output() -> Result<(), Box<dyn error::Error>> {
    let buf = Vec::new();
    let mut out = output::AnsiOutput::new(buf);
    println!("\nstart: {:?}", out);

    out.display("Hi")?;
    println!("{:?}", out);
    assert_eq!(out.stream_ref(), &[72, 105]);

    out.move_to_column(1)?;
    println!("{:?}", out);
    assert_eq!(out.stream_ref(), &[72, 105, 27, 91, 49, 68]);

    out.display("A")?;
    println!("{:?}", out);
    assert_eq!(out.stream_ref(), &[72, 105, 27, 91, 49, 68, 65]);

    out.move_to_column(2)?;
    println!("{:?}", out);
    assert_eq!(out.stream_ref(), &[72, 105, 27, 91, 49, 68, 65]);

    out.new_line()?;
    println!("{:?}", out);
    assert_eq!(out.stream_ref(), &[72, 105, 27, 91, 49, 68, 65, 10]);

    out.display("?")?;
    println!("{:?}", out);
    assert_eq!(out.stream_ref(), &[72, 105, 27, 91, 49, 68, 65, 10, 63]);

    out.move_to_column(10)?;
    println!("{:?}", out);
    assert_eq!(
        out.stream_ref(),
        &[72, 105, 27, 91, 49, 68, 65, 10, 63, 27, 91, 57, 67]
    );

    Ok(())
}
