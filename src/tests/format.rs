//! Test the actual formatting of input data.

/*
 * Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::error;

use crate::format;
use crate::output;

#[derive(Debug)]
struct Output {
    stage: usize,
}

impl Default for Output {
    fn default() -> Self {
        Self { stage: 0 }
    }
}

impl output::Output for Output {
    fn display(&mut self, msg: &str) -> Result<(), Box<dyn error::Error>> {
        if msg
            != match self.stage {
                1 => "00000000  ",
                3 | 5 | 18 | 20 | 29 | 31 => "|",
                7 => "54 68 69 73 20 69 73 20  61 20 74 65 73 74 2e ",
                9 => "This is a test.",
                11 => "54 ",
                13 => "T",
                16 => "00000010  ",
                22 => "68 69 73 20 69 73 20 6f  6e 6c 79 20 61 20 74 65 ",
                24 => "his is only a te",
                27 => "00000020  ",
                33 => "73 74 2e ",
                35 => "st.",
                _ => "no such message ought to be displayed",
            }
        {
            panic!("display({:?}) at stage {}", msg, self.stage);
        }

        self.stage += 1;
        Ok(())
    }

    fn move_to_column(&mut self, column: usize) -> Result<(), Box<dyn error::Error>> {
        if column
            != match self.stage {
                0 | 15 | 26 => 0,
                2 | 17 | 28 => 60,
                4 | 19 | 30 => 77,
                6 | 21 | 32 => 10,
                8 | 23 | 34 => 61,
                10 => 56,
                12 => 76,
                36 => 19,
                _ => usize::MAX,
            }
        {
            panic!("move_to_column({}) at stage {}", column, self.stage);
        }

        self.stage += 1;
        Ok(())
    }

    fn new_line(&mut self) -> Result<(), Box<dyn error::Error>> {
        if !match self.stage {
            14 | 25 => true,
            _ => false,
        } {
            panic!("newline() at stage {}", self.stage);
        }

        self.stage += 1;
        Ok(())
    }
}

#[test]
fn test_format() -> Result<(), Box<dyn error::Error>> {
    let out = Output::default();
    let state = format::State::new(out);
    println!("\nstart: {:?}", state);
    assert_eq!(state.output_ref().stage, 0);
    assert_eq!(state.offset, 0);

    let state = state.start_output()?;
    println!("after start_output(): {:?}", state);
    assert_eq!(state.output_ref().stage, 7);
    assert_eq!(state.offset, 0);

    let state = state.handle_input(b"This is a test.")?;
    println!("after handle_input(): {:?}", state);
    assert_eq!(state.output_ref().stage, 11);
    assert_eq!(state.offset, 15);

    let state = state.handle_input(b"This is only a test.")?;
    println!("after handle_input(): {:?}", state);
    assert_eq!(state.output_ref().stage, 37);
    assert_eq!(state.offset, 35);

    Ok(())
}
